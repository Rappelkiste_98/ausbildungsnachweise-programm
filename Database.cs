﻿using System;
using System.Data.SQLite;

public class sqlDatabase
{
	public sqlDatabase()
	{
		if(!File.Exists("database.db")) {
			SQLiteConnection.CreateFile("database.db");
		}
	}

	private SQLiteConnection createConnection() {
		SQLiteConnection sqlConnection = new SQLiteConnection("Data Source=database.db; Version=3;");
		sqlConnection.Open();

		return sqlConnection;
	}

	public void insertData()
    {
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = "insert into nachweise (KW, YEAR) values (1, 3000)";
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);
		sqlCommand.ExecuteNonQuery();

		sqlConnection.Close();
	}

}
