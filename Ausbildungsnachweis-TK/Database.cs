﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Windows;

public class sqlDatabase
{
	private string sDBFileName = "database.db";
	private string sDBFilePath;
	private Log lDBlog;

	public sqlDatabase()
	{
		this.sDBFilePath = OUT.AppDirectory() + this.sDBFileName;
		this.lDBlog = new Log("DB-LOG");

		string sCustomDB = JSON.GetDatabase();
		if(sCustomDB != "")
        {
			if (File.Exists(sCustomDB))
			{
				this.sDBFilePath = sCustomDB;
			}
			else
            {
				MessageBox.Show("Benutzerdefinierte Datenbank konnte nicht geladen werden. Programm startet mit Standart Datenbank!", "Datenbank nicht gefunden!", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		if (!OUT.CheckFileExists(this.sDBFileName))
        {
			CreateSQLDatabase();
		}
	}

	private void CreateSQLDatabase()
    {
		SQLiteConnection.CreateFile(this.sDBFilePath);

		SQLiteConnection sqlConnection = createConnection();

		string[] saDays = { "monday", "tuesday", "wednesday", "thursday", "friday" };

		string sSQL = "CREATE TABLE 'nachweise' ('ID' INTEGER, 'NR' INTEGER, 'CW' INTEGER, 'YEAR' INTEGER, 'WORKPLACE' TEXT, PRIMARY KEY('ID' AUTOINCREMENT));";
		string sSQL_days = "CREATE TABLE 'day_{0}' ('ID' INTEGER, 'NACHWEIS_ID' INTEGER, 'TIME_FROM' TEXT, 'TIME_UNTIL' TEXT, 'SPECIAL' TEXT, 'TEXT' TEXT, PRIMARY KEY('ID' AUTOINCREMENT));";

        foreach (var item in saDays)
        {
			sSQL = sSQL + string.Format(sSQL_days, item);
		}

		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			sqlCommand.ExecuteNonQuery();
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
		}

		sqlCommand.Cancel();
		sqlConnection.Close();
	}

	private SQLiteConnection createConnection()
	{
		SQLiteConnection sqlConnection = new SQLiteConnection(string.Format("Data Source={0}; Version=3;", this.sDBFilePath));
		sqlConnection.Open();

		return sqlConnection;
	}

	public List<string> select_allNachweise()
	{
		List<string> lsDatas = new List<string>();
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = "SELECT NR FROM nachweise ORDER BY NR DESC";
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			SQLiteDataReader reader = sqlCommand.ExecuteReader();
			while (reader.Read())
			{
				lsDatas.Add(Convert.ToString(reader["NR"]));
			}

			this.lDBlog.ToFile(string.Format("SELECT_ALLNACHWEISE :: {0} >> [{1}]", sSQL, OUT.ListResultString(lsDatas)));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("SELECT_ALLNACHWEISE :: {0} !!!ERROR!!! {1}", sSQL, ex));
			lsDatas = null;
		}

		sqlCommand.Cancel();
		sqlConnection.Close();

		return lsDatas;
	}

	public List<string> select_nachweisData(int iNr)
    {
		List<string> lsDatas = new List<string>();
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = "SELECT ID, NR, CW, YEAR, WORKPLACE FROM nachweise WHERE NR = " + iNr;
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			SQLiteDataReader reader = sqlCommand.ExecuteReader();

			if (reader.HasRows)
			{
				reader.Read();
				lsDatas.Add(Convert.ToString(reader["ID"]));
				lsDatas.Add(Convert.ToString(reader["NR"]));
				lsDatas.Add(Convert.ToString(reader["CW"]));
				lsDatas.Add(Convert.ToString(reader["YEAR"]));
				lsDatas.Add(Convert.ToString(reader["WORKPLACE"]));
			}

			this.lDBlog.ToFile(string.Format("SELECT_NACHWEISDATA :: {0} >> [{1}]", sSQL, OUT.ListResultString(lsDatas)));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("SELECT_NACHWEISDATA :: {0} !!!ERROR!!! {1}", sSQL, ex));
			lsDatas = null;
		}

		sqlCommand.Cancel();
		sqlConnection.Close();

		return lsDatas;
	}

	public List<string> select_nachweisData_Daily(int iID, string sDay)
    {
		List<string> lsDatas = new List<string>();
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = string.Format("SELECT TIME_FROM, TIME_UNTIL, SPECIAL, TEXT FROM day_{0} WHERE NACHWEIS_ID = {1}", sDay, iID);
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			SQLiteDataReader reader = sqlCommand.ExecuteReader();
			
			if(reader.HasRows)
            {
				reader.Read();
				lsDatas.Add(Convert.ToString(reader["TIME_FROM"]));
				lsDatas.Add(Convert.ToString(reader["TIME_UNTIL"]));
				lsDatas.Add(Convert.ToString(reader["SPECIAL"]));
				lsDatas.Add(Convert.ToString(reader["TEXT"]));
			}

			this.lDBlog.ToFile(string.Format("SELECT_NACHWEISDATA_DAILY :: {0} >> [{1}]", sSQL, OUT.ListResultString(lsDatas)));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("SELECT_NACHWEISDATA_DAILY :: {0} !!!ERROR!!! {1}", sSQL, ex));
			lsDatas = null;
		}

		sqlCommand.Cancel();
		sqlConnection.Close();

		return lsDatas;
	}

	public void update_nachweisData(int iID, int iNr, int iCW, int iYear, string sWorkplace)
	{
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = string.Format("UPDATE nachweise SET NR = '{0}', CW = '{1}' , YEAR = '{2}' , WORKPLACE = '{3}' WHERE ID = {4}", iNr, iCW, iYear, sWorkplace, iID);
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			int result = sqlCommand.ExecuteNonQuery();

			this.lDBlog.ToFile(string.Format("UPDATE_NACHWEISDATA :: {0} >> [{1}]", sSQL, result));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("UPDATE_NACHWEISDATA :: {0} !!!ERROR!!! {1}", sSQL, ex));
		}

		sqlCommand.Cancel();
		sqlConnection.Close();
	}

	public void update_nachweisData_Daily(int iNachweisID, string sDay, List<string> alDatas)
    {
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = string.Format("UPDATE day_{0} SET TIME_FROM = '{1}' , TIME_UNTIL = '{2}' , SPECIAL = '{3}' , TEXT = '{4}' WHERE NACHWEIS_ID = {5}", sDay, alDatas[0], alDatas[1], alDatas[2], alDatas[3], iNachweisID);
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			int result = sqlCommand.ExecuteNonQuery();

			this.lDBlog.ToFile(string.Format("UPDATE_NACHWEISDATA_DAILY :: {0} >> [{1}]", sSQL, result));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("UPDATE_NACHWEISDATA_DAILY :: {0} !!!ERROR!!! {1}", sSQL, ex));
		}

		sqlCommand.Cancel();
		sqlConnection.Close();
	}

	public string insert_nachweisData(int iNewNr, int iNewCW, int iNewYear, string sWorkplace)
	{
		Stopwatch swTimer = new Stopwatch();
		swTimer.Start();

		SQLiteConnection sqlConnection = createConnection();
		Settings jsonSettrings = JSON.GetSettings();

		string sSQL_General =
			"CREATE TEMP TABLE IF NOT EXISTS _Variables (tempID INT);" +
			string.Format("INSERT INTO nachweise(NR, CW, YEAR, WORKPLACE) VALUES({0}, {1}, {2}, '{3}');", iNewNr, iNewCW, iNewYear, sWorkplace) +
			"INSERT OR REPLACE INTO _Variables VALUES((SELECT last_insert_rowid()));";
		string sSQL_Days;
		string sSQL_DropTemp = "DROP TABLE _Variables;";

		if(jsonSettrings.lTimes != null)
        {
			sSQL_Days =
				string.Format("INSERT INTO day_monday(NACHWEIS_ID, TIME_FROM, TIME_UNTIL) VALUES((SELECT tempID FROM _Variables), '{0}', '{1}');", jsonSettrings.lTimes[0].sWorkFrom, jsonSettrings.lTimes[0].sWorkUntil) +
				string.Format("INSERT INTO day_tuesday(NACHWEIS_ID, TIME_FROM, TIME_UNTIL) VALUES((SELECT tempID FROM _Variables), '{0}', '{1}');", jsonSettrings.lTimes[1].sWorkFrom, jsonSettrings.lTimes[1].sWorkUntil) +
				string.Format("INSERT INTO day_wednesday(NACHWEIS_ID, TIME_FROM, TIME_UNTIL) VALUES((SELECT tempID FROM _Variables), '{0}', '{1}');", jsonSettrings.lTimes[2].sWorkFrom, jsonSettrings.lTimes[2].sWorkUntil) +
				string.Format("INSERT INTO day_thursday(NACHWEIS_ID, TIME_FROM, TIME_UNTIL) VALUES((SELECT tempID FROM _Variables), '{0}', '{1}');", jsonSettrings.lTimes[3].sWorkFrom, jsonSettrings.lTimes[3].sWorkUntil) +
				string.Format("INSERT INTO day_friday(NACHWEIS_ID, TIME_FROM, TIME_UNTIL) VALUES((SELECT tempID FROM _Variables), '{0}', '{1}');", jsonSettrings.lTimes[4].sWorkFrom, jsonSettrings.lTimes[4].sWorkUntil);
		} else
        {
			sSQL_Days =
				"INSERT INTO day_monday(NACHWEIS_ID) VALUES((SELECT tempID FROM _Variables));" +
				"INSERT INTO day_tuesday(NACHWEIS_ID) VALUES((SELECT tempID FROM _Variables));" +
				"INSERT INTO day_wednesday(NACHWEIS_ID) VALUES((SELECT tempID FROM _Variables));" +
				"INSERT INTO day_thursday(NACHWEIS_ID) VALUES((SELECT tempID FROM _Variables));" +
				"INSERT INTO day_friday(NACHWEIS_ID) VALUES((SELECT tempID FROM _Variables));";
		}

		SQLiteCommand sqlCommand = new SQLiteCommand((sSQL_General + sSQL_Days + sSQL_DropTemp), sqlConnection);

		try
		{
			int result = sqlCommand.ExecuteNonQuery();

			this.lDBlog.ToFile(string.Format("INSERT_NACHWEISDATA :: {0} >> [{1}]", (sSQL_General + sSQL_Days + sSQL_DropTemp), result));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("INSERT_NACHWEISDATA :: {0} !!!ERROR!!! {1}", (sSQL_General + sSQL_Days + sSQL_DropTemp), ex));
		}

		sqlCommand.Cancel();
		sqlConnection.Close();

		swTimer.Stop();
		string sTime = string.Format("{0:00}.{1:00} sec", swTimer.Elapsed.Seconds, swTimer.Elapsed.Milliseconds);
		Console.WriteLine("INSERT: " + sTime);
		return sTime;
	}

	public void remove_nachweisData(int iID)
    {
		SQLiteConnection sqlConnection = createConnection();

		string sSQL = string.Format("DELETE FROM nachweise WHERE ID = {0}; DELETE FROM day_monday WHERE ID = {0}; DELETE FROM day_tuesday WHERE NACHWEIS_ID = {0}; DELETE FROM day_wednesday WHERE NACHWEIS_ID = {0}; DELETE FROM day_thursday WHERE NACHWEIS_ID = {0}; DELETE FROM day_friday WHERE NACHWEIS_ID = {0};", iID);
		SQLiteCommand sqlCommand = new SQLiteCommand(sSQL, sqlConnection);

		try
		{
			int result = sqlCommand.ExecuteNonQuery();

			this.lDBlog.ToFile(string.Format("REMOVE_NACHWEISDATA :: {0} >> [{1}]", sSQL, result));
		}
		catch (SQLiteException ex)
		{
			Console.WriteLine("{0} Exception caught.", ex);
			this.lDBlog.ToFile(string.Format("REMOVE_NACHWEISDATA :: {0} !!!ERROR!!! {1}", sSQL, ex));
		}

		sqlCommand.Cancel();
		sqlConnection.Close();
	}
}