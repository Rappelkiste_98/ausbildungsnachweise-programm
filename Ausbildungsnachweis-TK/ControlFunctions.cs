﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

public class ControlFunctions
{
    public static void TextBox_Reset(TextBox[] tbaInputs)
    {
        foreach (TextBox input in tbaInputs)
        {
            input.Text = "";
        }
    }

    public static void TextBox_InputDatas(TextBox[] tbaInputs, List<string> lsDatas)
    {
        for (int i = 0; i < tbaInputs.Length; i++)
        {
            tbaInputs[i].Text = Convert.ToString(lsDatas[i]);
        }
    }

    public static List<string> TextBox_SelectDatas(TextBox[] tbaInputs)
    {
        List<string> lsDatas = new List<string>();

        foreach (TextBox input in tbaInputs)
        {
            lsDatas.Add(input.Text);
        }

        return lsDatas;
    }

    public static void ListBox_Fill(ListBox lbList, List<string> lsDatas)
    {
        foreach (var item in lsDatas)
        {
            lbList.Items.Add(Convert.ToString(item));
        }
    }

    public static void ListBox_Reset(ListBox lbList)
    {
        for (int i = (lbList.Items.Count - 1); i >= 0; --i)
        {
            lbList.Items.RemoveAt(i);
        }
    }

    public static void TextBox_IntFormat(TextBox tbInput)
    {
        char[] caAllowed = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        string sText = tbInput.Text;
        char[] caArray = sText.ToCharArray();
        string sFilter = "";

        foreach (var item in caArray)
        {
            if(caAllowed.Contains(item)) {
                sFilter += item;
            }
        }

        tbInput.Text = sFilter;
    }

    public static void TextBox_TimeFormat(TextBox tbInput)
    {
        string sFinalText = "";
        string sText = tbInput.Text;

        char[] caAllowed = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':' };
        char[] caArray = sText.ToCharArray();
        string sFilter = "";

        foreach (var item in caArray)
        {
            if (caAllowed.Contains(item))
            {
                sFilter += item;
            }
        };

        if(TimeSpan.TryParse(sFilter, out var dummyOutput))
        {
            tbInput.Text = sFilter;
        }
        else
        {
            tbInput.Text = "00:00";
        }
    }
}

