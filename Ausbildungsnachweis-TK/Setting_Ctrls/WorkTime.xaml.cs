﻿using System.Windows;
using System.Windows.Controls;

namespace Ausbildungsnachweis_TK.Setting_Ctrls
{
    /// <summary>
    /// Interaktionslogik für WorkTime.xaml
    /// </summary>
    public partial class WorkTime : UserControl
    {
        public WorkTime()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public string Text { get; set; }

        private void CheckFormat(object sender, RoutedEventArgs e)
        {
            TextBox tbInput = (TextBox)sender;

            if (tbInput.Text != "")
            {
                ControlFunctions.TextBox_TimeFormat((TextBox)sender);
            }
        }
    }
}
