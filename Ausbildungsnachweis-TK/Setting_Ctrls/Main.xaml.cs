﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ausbildungsnachweis_TK.Setting_Ctrls
{
    /// <summary>
    /// Interaktionslogik für Main.xaml
    /// </summary>
    public partial class Main : UserControl
    {
        public Main()
        {

            InitializeComponent();
            LoadSettings();
        }

        public UserControl ctrlEditor { get; set; }
        public Log lLogs { get; set; }
        public Foot fDebug { get; set; }

        public void OpenSettings()
        {
            this.ctrlEditor.Visibility = Visibility.Hidden;
            this.Visibility = Visibility.Visible;
        }

        public void CloseSettings()
        {
            SaveSettings();
            this.ctrlEditor.Visibility = Visibility.Visible;
            this.Visibility = Visibility.Hidden;
        }

        private void LoadSettings()
        {
            Settings jsonSettings = JSON.GetSettings();

            tbName.Text = jsonSettings.sName;
            tbFamilyname.Text = jsonSettings.sFamilyName;
            tbJob.Text = jsonSettings.sJob;
            tbShortJob.Text = jsonSettings.sShortJob;
            tbYear.Text = jsonSettings.sYear;
            tbTeacher.Text = jsonSettings.sTeacher;
            tbTel.Text = jsonSettings.sTel;
            tbDatabase.Text = jsonSettings.sDatabase;

            if (jsonSettings.lTimes != null)
            {
                WorkTime[] wtaAll = { wtMonday, wtTuesday, wtWednesday, wtThursday, wtFriday };
                for (int i = 0; i < (jsonSettings.lTimes.Count); i++)
                {
                    wtaAll[i].tbWorkFrom.Text = jsonSettings.lTimes[i].sWorkFrom;
                    wtaAll[i].tbWorkUntil.Text = jsonSettings.lTimes[i].sWorkUntil;
                }
            }
        }

        private void SaveSettings()
        {
            Settings sSettings = new Settings();

            sSettings.sName = tbName.Text;
            sSettings.sFamilyName = tbFamilyname.Text;
            sSettings.sJob = tbJob.Text;
            sSettings.sShortJob = tbShortJob.Text;
            sSettings.sYear = tbYear.Text;
            sSettings.sTeacher = tbTeacher.Text;
            sSettings.sTel = tbTel.Text;
            sSettings.sDatabase = tbDatabase.Text;

            List<Settings_Times> lTempTimes = new List<Settings_Times>();

            WorkTime[] wtaAll = { wtMonday, wtTuesday, wtWednesday, wtThursday, wtFriday };
            foreach (var item in wtaAll)
            {
                Settings_Times timesItem = new Settings_Times();

                timesItem.sWorkFrom = item.tbWorkFrom.Text;
                timesItem.sWorkUntil = item.tbWorkUntil.Text;
                lTempTimes.Add(timesItem);
            }

            sSettings.lTimes = lTempTimes;
            string sJson = JsonConvert.SerializeObject(sSettings);
            JSON.SaveSettings(sJson);

            fDebug.SetInfo("Einstellungen wurden gespeichert", "SUCCESS");
        }


        private void Click_SelectDatabaseFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Datenbank (*.db)|*.db|Alle Dateien (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                tbDatabase.Text = openFileDialog.FileName;
            }
        }

        private void Click_Close(object sender, RoutedEventArgs e)
        {
            CloseSettings();
        }

    }
}
