﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ausbildungsnachweis_TK
{
    /// <summary>
    /// Interaktionslogik für Foot.xaml
    /// </summary>
    public partial class Foot : UserControl
    {
        private int iTaskRun = 0;

        public Foot()
        {
            InitializeComponent();
        }

        public void SetVersion(string sVersion)
        {
            output_version.Text = sVersion;
        }

        public void SetInfo(string sText, string sMode)
        {
            iTaskRun++;
            int iThisTask = iTaskRun;

            switch (sMode)
            {
                case "ERROR": Blue.Color = (Color)ColorConverter.ConvertFromString("#FFF64E00"); break;
                case "WORK": Blue.Color = (Color)ColorConverter.ConvertFromString("#FFF6BE00"); break;
                case "SUCCESS": Blue.Color = (Color)ColorConverter.ConvertFromString("#00a0f6"); break;
                default: Blue.Color = (Color)ColorConverter.ConvertFromString("#00a0f6"); break;
            }

            output_debug.Text = sText;
        }
    }
}
