﻿using System.Threading.Tasks;
using System.Windows;
using Microsoft.Office.Interop.Excel;

public class AppExcel
{
    private string sPath;
    private _Application appExcel = new Microsoft.Office.Interop.Excel.Application();
    private Workbook wbExcel;
    private Worksheet wsExcel;

    private void Close()
    {
        wbExcel.Close();
    }

    public async Task openExcel(string sPath, int iSheet)
    {
        this.sPath = sPath;

        this.wbExcel = await Task.Run(() => appExcel.Workbooks.Open(this.sPath));
        this.wsExcel = await Task.Run(() => this.wbExcel.Worksheets[iSheet]);
    }

    public string ReadCell(int iX, int iY)
    {
        string sValue = this.wsExcel.Cells[iY, iX].Value;
        return sValue;
    }

    public void WriteCell(int iX, int iY, string sValue)
    {
        this.wsExcel.Cells[iY, iX].Value = sValue;
    }

    public async Task SaveAs(string sNewFileName)
    {
        await Task.Run(() => wbExcel.SaveAs(sNewFileName));

        Close();
    }

    public void Save()
    {
        wbExcel.Save();

        Close();
    }

    public async Task PrintAsPDF(string sPath)
    {
        await Task.Run(() =>
        {
            try
            {
                wbExcel.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, sPath);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show("Ausbildungsnachweis konnte nicht exportiert werden, da die Datei eventuell noch geöffnet ist.", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        });
    }
}