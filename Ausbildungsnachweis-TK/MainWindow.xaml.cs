﻿using System.Windows;
using MahApps.Metro.Controls;
using System.Reflection;
using System.Diagnostics;
using System;

namespace Ausbildungsnachweis_TK
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private Log lLogs = new Log("LOG");
        public string sVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public MainWindow()
        {
            InitializeComponent();
            InitializeDebug();

            WindowFoot.SetVersion(this.sVersion);
            this.lLogs.ToFile("Program Version: " + this.sVersion);

            if (!OUT.CheckFileExists("settings.json"))
            {
                ctrlSettings.OpenSettings();
            }
        }

        private void InitializeDebug()
        {
            ctrlSettings.lLogs = lLogs;
            ctrlSettings.fDebug = WindowFoot;
            ctrlSettings.ctrlEditor = ctrlEditor;

            ctrlEditor.lLogs = lLogs;
            ctrlEditor.fDebug = WindowFoot;
        }

        private void Click_SwitchSettings(object sender, RoutedEventArgs e)
        {
            switch (ctrlSettings.Visibility)
            {
                case Visibility.Visible:
                    ctrlSettings.CloseSettings();
                    break;
                case Visibility.Hidden:
                    ctrlSettings.OpenSettings();
                    break;
                default:
                    break;
            }

        }

        private void OpenFolder(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", OUT.AppDirectory());
        }

        private void OpenInfos(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Ausbildungsnachweise " + this.sVersion + Environment.NewLine + Environment.NewLine + "Konktakt:" + Environment.NewLine + "Niklas Quenter" + Environment.NewLine + "thyssenkrupp Steel Europe AG" + Environment.NewLine + "niklas.quenter@thyssenkrupp.com" + Environment.NewLine + Environment.NewLine + "Quelltext: https://bitbucket.org/Rappelkiste_98/ausbildungsnachweise-programm", "Informationen", MessageBoxButton.OK);
        }
    }
}
