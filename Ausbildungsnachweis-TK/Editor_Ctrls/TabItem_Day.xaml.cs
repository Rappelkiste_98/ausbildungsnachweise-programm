﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace Ausbildungsnachweis_TK.Editor_Ctrls
{
    /// <summary>
    /// Interaktionslogik für TabItem_Day.xaml
    /// </summary>
    public partial class TabItem_Day : UserControl
    {
        public TabItem_Day()
        {
            this.DataContext = this;

            InitializeComponent();
        }

        public TextBox tbWorkplace { get; set; }
        public sqlDatabase sqlDB { get; set; }
        public Log lLogs { get; set; }
        public Foot fDebug { get; set; }
        public string TabID { get; set; }

        private TimeSpan CalculatWorktime(string sFrom, string sUntil)
        {
            DateTime dtFrom = Convert.ToDateTime(sFrom);
            DateTime dtUntil = Convert.ToDateTime(sUntil);

            TimeSpan tsWorktime = dtUntil - dtFrom;
            TimeSpan ts6Hours = new TimeSpan(6, 0, 0);
            TimeSpan ts9Hours = new TimeSpan(9, 0, 0);

            if (tsWorktime >= ts6Hours && tsWorktime < ts9Hours)
            {
                TimeSpan tsBreak = new TimeSpan(0, 30, 0);
                tsWorktime -= tsBreak;
            }
            else if(tsWorktime >= ts9Hours)
            {
                TimeSpan tsBreak = new TimeSpan(0, 45, 0);
                tsWorktime -= tsBreak;
            }
            else
            {

            }

            return tsWorktime;
        }

        public void FillWorkTime()
        {
            int iIndexWorkTime = 0;
            switch (this.Name)
            {
                case "tabMonday": iIndexWorkTime = 0; break;
                case "tabTuesday": iIndexWorkTime = 1; break;
                case "tabWednesday": iIndexWorkTime = 2; break;
                case "tabThursday": iIndexWorkTime = 3; break;
                case "tabFriday": iIndexWorkTime = 4; break;
                default: break;
            }
            string[] saWorktime = JSON.GetDailyWorktimes(iIndexWorkTime);

            if (tbTimeFrom.Text == "")
            {
                tbTimeFrom.Text = saWorktime[0];
                tbTimeUntil.Text = saWorktime[1];
            }
        }

        public async Task NachweisLoad(int iNachweisID)
        {
            TextBox[] tbaInputs = { tbTimeFrom, tbTimeUntil, tbSpecial, tbText };
            ControlFunctions.TextBox_Reset(tbaInputs);

            List<string> lsDatas = await Task.Run(() => sqlDB.select_nachweisData_Daily(iNachweisID, TabID));

            if (lsDatas != null)
            {
                ControlFunctions.TextBox_InputDatas(tbaInputs, lsDatas);
            } else
            {
                fDebug.SetInfo("Fehler beim Laden des Nachweises", "ERROR");
                lLogs.ToFile("!!ERROR by NACHWEISLOAD -> " + this.Name + "!! No Datas from Database");
            }
        }

        public async Task NachweisSave(int iNachweisID)
        {
            TextBox[] tbaInputs = { tbTimeFrom, tbTimeUntil, tbSpecial, tbText };

            List<string> lsDatas = ControlFunctions.TextBox_SelectDatas(tbaInputs);

            await Task.Run(() => sqlDB.update_nachweisData_Daily(iNachweisID, TabID, lsDatas));
        }

        public void NachweisExport(AppExcel aeTemp)
        {
            TimeSpan tsWorktime;
            TextBox[] tbaInputs = { tbTimeFrom, tbTimeUntil, tbSpecial, tbText };
            List<string> lsDatas = ControlFunctions.TextBox_SelectDatas(tbaInputs);

            int iRowText = -1;
            int iRowTable = -1;
            switch (TabID)
            {
                case "monday": iRowTable = 16; iRowText = 23; break;
                case "tuesday": iRowTable = 17; iRowText = 29; break;
                case "wednesday": iRowTable = 18; iRowText = 35; break;
                case "thursday": iRowTable = 19; iRowText = 41; break;
                case "friday": iRowTable = 20; iRowText = 47; break;
                default: break;
            }

            if(lsDatas[0] != "" && lsDatas[1] != "")
            {
                tsWorktime = CalculatWorktime(lsDatas[0], lsDatas[1]);
                aeTemp.WriteCell(4, iRowTable, string.Format("{0}:{1}", tsWorktime.Hours, tsWorktime.Minutes));
            }

            aeTemp.WriteCell(2, iRowTable, lsDatas[0]);
            aeTemp.WriteCell(3, iRowTable, lsDatas[1]);
            aeTemp.WriteCell(6, iRowTable, lsDatas[2]);

            aeTemp.WriteCell(1, iRowText, lsDatas[3]);
        }

        private void ActivateSpecial(object sender, RoutedEventArgs e)
        {
            TextBox tbSender = (TextBox)sender;

            switch (tbSender.Text)
            {
                case "K": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbText.Text = "Krankheit"; break;
                case "TU": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbText.Text = "Tarifurlaub"; break;
                case "AZV": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbText.Text = "AZV"; break;
                case "SU": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbText.Text = "Sonderurlaub"; break;
                case "BS": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbWorkplace.Text = "Berufsschule"; break;
                case "S": FillWorkTime(); tbWorkplace.Text = "Seminar"; break;
                case "F": tbTimeFrom.Text = ""; tbTimeUntil.Text = ""; tbText.Text = "Feiertag"; break;
                default: FillWorkTime(); break;
            }
        }

        private void Check_TimeFormat(object sender, RoutedEventArgs e)
        {
            TextBox tbInput = (TextBox)sender;

            if(tbInput.Text != "")
            {
                ControlFunctions.TextBox_TimeFormat((TextBox)sender);
            }
        }


    }
}
