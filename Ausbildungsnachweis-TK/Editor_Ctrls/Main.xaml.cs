﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace Ausbildungsnachweis_TK.Editor_Ctrls
{
    /// <summary>
    /// Interaktionslogik für Main.xaml
    /// </summary>
    public partial class Main : UserControl
    {
        private bool bActionRun;
        private sqlDatabase sqlDB;

        public Main()
        {
            this.bActionRun = false;
            this.sqlDB = new sqlDatabase();

            InitializeComponent();
            InitializeVariables();

            int iYear = DateTime.Now.Year;
            DateTime dtLastDay = new DateTime(iYear, 12, 31);
            tbCW.ToolTip = (string)tbCW.ToolTip + OUT.calendarWeekNow();

            RefreshList(true);
        }

        public Log lLogs { get; set; }
        public Foot fDebug { get; set; }

        private async void InitializeVariables()
        {
            TabItem_Day[] tabArray = { tabMonday, tabTuesday, tabWednesday, tabThursday, tabFriday };

            await Task.Run(() =>
            {
                while(fDebug == null) { }
            });

            foreach (var item in tabArray)
            {
                item.tbWorkplace = this.tbWorkplace;
                item.fDebug = this.fDebug;
                item.lLogs = this.lLogs;
                item.sqlDB = this.sqlDB;
            }
        }

        private bool CheckNrExists(int iCheckNr)
        {
            ListBox lbName = lbNachweise;
            bool bError = false;

            for (int index = lbName.Items.Count - 1; index >= 0; --index)
            {
                int iItemNr = Convert.ToInt32(lbName.Items.GetItemAt(index));
                if (iItemNr == iCheckNr)
                {
                    bError = true;
                }
            }

            return bError;
        }

        private async void RefreshList(bool bSelectNewest)
        {
            if (lbNachweise.Items.Count > 0)
            {
                ControlFunctions.ListBox_Reset(lbNachweise);
            }

            List<string> lsNachweise = await Task.Run(() => sqlDB.select_allNachweise());

            if (lsNachweise != null)
            {
                ControlFunctions.ListBox_Fill(lbNachweise, lsNachweise);

                if (bSelectNewest && lbNachweise.Items.Count > 0)
                {
                    lbNachweise.SelectedItem = lbNachweise.Items[0];
                }
            }

            lLogs.ToFile("REFRESHLIST: successful refreshed [" + lsNachweise.Count + " Datas]");
        }

        private async void NachweisAdd()
        {
            this.bActionRun = true;
            fDebug.SetInfo("Nachweis wird hinzugefügt ...", "WORK");

            int iNewNr = 1;
            int iNewCW = OUT.calendarWeekNow();
            int iNewYear = OUT.currentYear();
            string sNewWorkplace = "";

            if (lbNachweise.Items.Count > 0)
            {
                //int iNewstNr = Convert.ToInt32(lbNachweise.Items[0]);
                //List<string> lsDatas = await Task.Run(() => sqlDB.select_nachweisData(iNewstNr));

                //iNewNr = iNewstNr + 1;
                //iNewCW = Convert.ToInt32(lsDatas[2]) + 1;
                //iNewYear = Convert.ToInt32(lsDatas[3]);
                //sNewWorkplace = lsDatas[4];

                iNewNr = Convert.ToInt32(this.tbNr.Text) + 1;
                iNewCW = Convert.ToInt32(this.tbCW.Text) + 1;
                iNewYear = Convert.ToInt32(this.tbYear.Text);
                sNewWorkplace = this.tbWorkplace.Text;

                //Thread.Sleep(500);
            }

            string sTime = await Task.Run(() => sqlDB.insert_nachweisData(iNewNr, iNewCW, iNewYear, sNewWorkplace));

            RefreshList(true);

            fDebug.SetInfo("Nachweis mit der Nr. " + iNewNr + " hinzugefügt", "SUCCESS");
            lLogs.ToFile("NACHWEISADD: successful new Nachweis added [NR " + iNewNr + "] <== " + sTime);

            this.bActionRun = false;
        }

        private async void NachweisDelete()
        {
            this.bActionRun = true;

            fDebug.SetInfo("Nachweis wird gelöscht ...", "WORK");

            if (lbNachweise.Items.Count > 0)
            {
                int iID = Convert.ToInt32(tbID.Text);

                MessageBoxResult mbrDelete = MessageBox.Show("Möchten Sie den Nachweis-Nr. " + lbNachweise.SelectedItem + " wirklich Löschen?", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (mbrDelete == MessageBoxResult.Yes)
                {
                    await Task.Run(() => sqlDB.remove_nachweisData(iID));

                    fDebug.SetInfo("Nachweis mit der Nr. " + lbNachweise.SelectedItem + " gelöscht", "SUCCESS");
                    lLogs.ToFile("NACHWEISDELETE: successful Nachweis deleted [NR " + lbNachweise.SelectedItem + "]");
                }
                else
                {
                    fDebug.SetInfo("Nachweis mit der Nr. " + lbNachweise.SelectedItem + " wurde nicht gelöscht!", "ERROR");
                    lLogs.ToFile("NACHWEISDELETE: Nachweis delete canceled [NR " + lbNachweise.SelectedItem + "]");
                }
            }

            RefreshList(true);

            this.bActionRun = false;
        }

        private async void NachweisLoad(int iNachweisnr)
        {
            TextBox[] tbaGeneral = { tbID, tbNr, tbCW, tbYear, tbWorkplace };
            ControlFunctions.TextBox_Reset(tbaGeneral);

            if (iNachweisnr > 0)
            {
                List<string> alDatas = await Task.Run(() => sqlDB.select_nachweisData(iNachweisnr));

                if (alDatas != null)
                {
                    ControlFunctions.TextBox_InputDatas(tbaGeneral, alDatas);
                    lLogs.ToFile("NACHWEISLOAD: successful loaded Datas [NR " + iNachweisnr + "]");

                    await tabMonday.NachweisLoad(Convert.ToInt32(alDatas[0])); 
                    await tabTuesday.NachweisLoad(Convert.ToInt32(alDatas[0]));
                    await tabWednesday.NachweisLoad(Convert.ToInt32(alDatas[0]));
                    await tabThursday.NachweisLoad(Convert.ToInt32(alDatas[0]));
                    await tabFriday.NachweisLoad(Convert.ToInt32(alDatas[0]));

                    //fDebug.SetInfo("Nachweis mit Nr. " + iNachweisnr + " geladen", "SUCCESS");
                }
                else
                {
                    fDebug.SetInfo("Fehler beim Laden des Nachweises", "ERROR");
                    lLogs.ToFile("!!ERROR by NACHWEISLOAD!! No Datas from Database [NR" + iNachweisnr + "]");
                }
            }
        }

        private async void NachweisSave(int iOldNr)
        {
            this.bActionRun = true;

            fDebug.SetInfo("Nachweis wird gespeichert ...", "WORK");

            TextBox[] tbaInputs = { tbID, tbNr, tbCW, tbYear, tbWorkplace };
            List<string> lsDatas = ControlFunctions.TextBox_SelectDatas(tbaInputs);

            int iID = Convert.ToInt32(lsDatas[0]);
            int iNr = Convert.ToInt32(lsDatas[1]);

            if (CheckNrExists(iNr) && iOldNr != iNr)
            {
                MessageBox.Show("Diese Nachweis-Nr ist schon vergeben!");
                fDebug.SetInfo("Nachweis-Nr schon vergeben", "ERROR");
                lLogs.ToFile("!!ERROR by NACHWEISSAVE!! Nr not available [NR " + iNr + "]");
            }
            else
            {
                await Task.Run(() => sqlDB.update_nachweisData(Convert.ToInt32(lsDatas[0]), Convert.ToInt32(lsDatas[1]), Convert.ToInt32(lsDatas[2]), Convert.ToInt32(lsDatas[3]), lsDatas[4]));

                await tabMonday.NachweisSave(Convert.ToInt32(lsDatas[0]));
                await tabTuesday.NachweisSave(Convert.ToInt32(lsDatas[0]));
                await tabWednesday.NachweisSave(Convert.ToInt32(lsDatas[0]));
                await tabThursday.NachweisSave(Convert.ToInt32(lsDatas[0]));
                await tabFriday.NachweisSave(Convert.ToInt32(lsDatas[0]));

                if (iOldNr != iNr)
                {
                    RefreshList(false);
                }

                lLogs.ToFile("NACHWEISSAVE: successful saved Datas [NR" + iNr + "]");
                fDebug.SetInfo("Nachweis erfolgreich gespeichert", "SUCCESS");
            }

            this.bActionRun = false;
        }

        private async void NachweisExport()
        {
            this.bActionRun = true;

            fDebug.SetInfo("Nachweis wird exportiert ...", "WORK");

            if(!File.Exists(OUT.AppDirectory() +"Vorlage.xlsx"))
            {
                File.Copy("Vorlage.xlsx", OUT.AppDirectory() + "Vorlage.xlsx");
            }

            Settings sJSON = JSON.GetSettings();
            AppExcel aeTemplate = new AppExcel();
            await aeTemplate.openExcel(OUT.AppDirectory() + "Vorlage.xlsx", 1);
            List<string> lsWeek = OUT.calWeekToDate(Convert.ToInt32(tbCW.Text), Convert.ToInt32(tbYear.Text));

            aeTemplate.WriteCell(9, 1, tbNr.Text);
            aeTemplate.WriteCell(7, 3, lsWeek[0].ToString());
            aeTemplate.WriteCell(10, 3, lsWeek[1].ToString());
            aeTemplate.WriteCell(7, 5, sJSON.sJob);
            aeTemplate.WriteCell(1, 7, string.Format("{0}, {1}", sJSON.sFamilyName, sJSON.sName));
            aeTemplate.WriteCell(7, 7, string.Format("{0} {1}", sJSON.sShortJob, sJSON.sYear));
            aeTemplate.WriteCell(1, 10, tbWorkplace.Text);
            aeTemplate.WriteCell(7, 10, sJSON.sTeacher);
            aeTemplate.WriteCell(10, 10, sJSON.sTel);

            tabMonday.NachweisExport(aeTemplate);
            tabTuesday.NachweisExport(aeTemplate);
            tabWednesday.NachweisExport(aeTemplate);
            tabThursday.NachweisExport(aeTemplate);
            tabFriday.NachweisExport(aeTemplate);


            await aeTemplate.PrintAsPDF(string.Format("{0}Ausbildungsnachweis-Nr_{1}.pdf", OUT.AppDirectory(), tbNr.Text));
            await aeTemplate.SaveAs(OUT.AppDirectory() + "temp.xlsx");

            File.Delete(OUT.AppDirectory() + "temp.xlsx");

            lLogs.ToFile("NACHWEISEXPORT: successful exported to '" + OUT.AppDirectory() + "'[NR" + tbNr.Text + "]");
            fDebug.SetInfo("Nachweis erfolgreich exportiert", "SUCCESS");

            this.bActionRun = false;
        }

        private void Selection_Load(object sender, SelectionChangedEventArgs e)
        {
            ListBox lbSender = (ListBox)sender;
            int iNachweisnr = Convert.ToInt32(lbSender.SelectedItem);

            NachweisLoad(iNachweisnr);
            tiMonday.Focus();
        }

        private void Click_Add(object sender, RoutedEventArgs e)
        {
            if (!this.bActionRun)
            {
                NachweisAdd();
            }
            else
            {
                MessageBox.Show("Es wird bereits eine Aufgabe bearbeitet!");
                lLogs.ToFile("!!ERROR by CLICK_ADD!! Another Task is running");
            }
        }

        private void Click_Delete(object sender, RoutedEventArgs e)
        {
            if (!this.bActionRun)
            {
                NachweisDelete();
            }
            else
            {
                MessageBox.Show("Es wird bereits eine Aufgabe bearbeitet!");
                lLogs.ToFile("!!ERROR by CLICK_DELETE!! Another Task is running");
            }
        }

        private void Click_Save(object sender, RoutedEventArgs e)
        {
            int iOldNr = Convert.ToInt32(lbNachweise.SelectedItem);

            if (!this.bActionRun)
            {
                NachweisSave(iOldNr);
            }
            else
            {
                MessageBox.Show("Es wird bereits eine Aufgabe bearbeitet!");
                lLogs.ToFile("!!ERROR by CLICK_SAVE!! Another Task is running");
            }
        }

        private void Click_Export(object sender, RoutedEventArgs e)
        {
            if (!this.bActionRun)
            {
                NachweisExport();
            }
            else
            {
                MessageBox.Show("Es wird bereits eine Aufgabe bearbeitet!");
                lLogs.ToFile("!!ERROR by CLICK_EXPORT!! Another Task is running");
            }
        }

        private void Check_IntFormat(object sender, RoutedEventArgs e)
        {
            ControlFunctions.TextBox_IntFormat((TextBox)sender);
        }

    }
}
