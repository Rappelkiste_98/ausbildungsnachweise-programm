﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

public class OUT
{
    public static DateTime timestampNow()
    {
        DateTime time = DateTime.Now;

        return time;
    }

    public static int currentYear()
    {
        DateTime time = DateTime.Now;

        return time.Year;
    }

    public static int calendarWeek(DateTime dtDate)
    {
        CultureInfo myCI = new CultureInfo("de-DE");
        System.Globalization.Calendar myCal = myCI.Calendar;

        CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
        DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

        return myCal.GetWeekOfYear(dtDate, myCWR, myFirstDOW);
    }

    public static int calendarWeekNow()
    {
        DateTime dtNow = DateTime.Now;

        return calendarWeek(dtNow);
    }

    public static List<string> calWeekToDate(int iCalWeek, int iYear)
    {
        List<string> lsResult = new List<string>();
        int iDays = 0;

        CultureInfo myCI = new CultureInfo("de-DE");
        System.Globalization.Calendar myCal = myCI.Calendar;
        CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
        DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

        DateTime dtFirstDay = new DateTime(iYear, 1, 1, myCal);
        int iCWFirstDayInYear = myCal.GetWeekOfYear(dtFirstDay, myCWR, myFirstDOW);
        int iCWFirstDay = (iCalWeek * 7) - 6;

        int iDayIndex = Convert.ToInt32(dtFirstDay.DayOfWeek);
        if(iDayIndex == 0)
        {
            iDayIndex = 7;
        }

        if(iCWFirstDayInYear != 1)
        {
            iDays = iCWFirstDay + (7 - iDayIndex);
        }
        else
        {
            iDays = iCWFirstDay - iDayIndex;
        }

        lsResult.Add(dtFirstDay.AddDays(iDays).ToShortDateString());
        lsResult.Add(dtFirstDay.AddDays(iDays + 4).ToShortDateString());
        return lsResult;
    }

    public static string AppDirectory()
    {
        string sAppName = Assembly.GetExecutingAssembly().GetName().Name.ToString();
        string sUserFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        string sDirectory = "Documents";

        return string.Format(@"{0}\{1}\{2}\", sUserFolder, sDirectory, sAppName);
    }

    public static bool CheckFileExists(string sFileName)
    {
        string sFilePath = AppDirectory() + sFileName;

        return File.Exists(sFilePath);
    }

    public static string ListResultString(List<string> list)
    {
        StringBuilder builder = new StringBuilder();

        foreach (string sitem in list)
        {
            builder.Append(sitem).Append("|");
        }
        
        return builder.ToString();
    }
}

public class Log
{
    private string sDirectory;
    private string sFile;
    
    public Log(string sLogFile)
    {
        this.sDirectory = @"Logs\";

        DateTime time = DateTime.Now;
        string sDate = time.ToString("yyyy.MM.dd H-mm-ss");

        Directory.CreateDirectory(OUT.AppDirectory() + sDirectory);

        this.sFile = (OUT.AppDirectory() + sDirectory + sLogFile + "_" + sDate + ".log");
    }

    private string createLogMessage(string sMessage)
    {
        return (OUT.timestampNow() + " | " + sMessage);
    }

    public void ToFile(string sMessage)
    {
        List<string> lsTest = new List<string>();
        lsTest.Add(createLogMessage(sMessage));

        File.AppendAllLines(sFile, lsTest);
    }

    public void ToConsole(String sMessage)
    {
        Console.WriteLine(createLogMessage(sMessage));
    }
}
