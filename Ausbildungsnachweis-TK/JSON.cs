﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

public class Settings
{
    public string sName { get; set; }
    public string sFamilyName { get; set; }
    public string sJob { get; set; }
    public string sShortJob { get; set; }
    public string sYear { get; set; }
    public string sTeacher { get; set; }
    public string sTel { get; set; }
    public string sDatabase { get; set; }

    public List<Settings_Times> lTimes { get; set; }
}

public class Settings_Times
{
    public string sWorkFrom { get; set; }
    public string sWorkUntil { get; set; }
}

class JSON
{
    public static Settings GetSettings()
    {
        JSON json = new JSON();
        Settings sSettings;

        if (OUT.CheckFileExists("settings.json"))
        {
            string sJson = File.ReadAllText(OUT.AppDirectory() + "settings.json");
            sSettings = JsonConvert.DeserializeObject<Settings>(sJson);
        } else
        {
            sSettings = new Settings();
        }

        return sSettings;
    }

    public static string GetDatabase()
    {
        JSON json = new JSON();
        string sDBPath = "";

        if (OUT.CheckFileExists("settings.json"))
        {
            string sJson = File.ReadAllText(OUT.AppDirectory() + "settings.json");
            Settings sSettings = JsonConvert.DeserializeObject<Settings>(sJson);

            sDBPath = sSettings.sDatabase;
        }

        return sDBPath;
    }

    public static string[] GetDailyWorktimes(int iGetIndex)
    {
        JSON json = new JSON();
        string[] saWorktime = new string[2];

        if (OUT.CheckFileExists("settings.json"))
        {
            string sJson = File.ReadAllText(OUT.AppDirectory() + "settings.json");
            Settings sSettings = JsonConvert.DeserializeObject<Settings>(sJson);

            saWorktime[0] = sSettings.lTimes[iGetIndex].sWorkFrom;
            saWorktime[1] = sSettings.lTimes[iGetIndex].sWorkUntil;
        }

        return saWorktime;
    }

    public static void SaveSettings(string sJSON)
    {
        File.WriteAllText(OUT.AppDirectory() + "settings.json", sJSON);
    }

}
